package com.example.scrumpokercards.adapter;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.scrumpokercards.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CardDeckAdapter extends BaseAdapter {

    public interface Listener {
        void onItemClicked(@NonNull String item);
    }


    private LayoutInflater layoutInflater;
    private Listener listener;
    private List<String> data;


    public CardDeckAdapter(@NonNull LayoutInflater layoutInflater, @NonNull List<String> data, @NonNull Listener listener) {
        this.layoutInflater = layoutInflater;
        this.data = data;
        this.listener = listener;
    }


    public void addItem(@NonNull String item) {
        data.add(item);
        notifyDataSetChanged();
    }

    public void deleteItem(@NonNull String item) {
        data.remove(item);
        notifyDataSetChanged();
    }


    @NonNull
    public List<String> getData() {
        return data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_card_list, parent, false);
        }

        String item = data.get(position);

        convertView.setOnClickListener(v -> listener.onItemClicked(item));

        TextView textView = convertView.findViewById(R.id.card_item_text_view);
        textView.setText(item);

        return convertView;
    }
}
