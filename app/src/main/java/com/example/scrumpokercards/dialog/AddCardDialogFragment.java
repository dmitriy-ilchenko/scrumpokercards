package com.example.scrumpokercards.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.scrumpokercards.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class AddCardDialogFragment extends DialogFragment {

    public interface Listener {
        void onCardAdded(@NonNull String card);
    }

    private Listener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (Listener) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());

        View dialogView = layoutInflater.inflate(R.layout.dialog_add_card, null, false);
        EditText cardEditText = dialogView.findViewById(R.id.card_edit_text);

        return new AlertDialog.Builder(getActivity())
                .setView(dialogView)
                .setTitle(R.string.custom_card_deck_add_card_dialog_title)
                .setPositiveButton(R.string.custom_card_deck_add_card_dialog_positive_button, (dialog, which) -> listener.onCardAdded(cardEditText.getText().toString()))
                .setNegativeButton(R.string.custom_card_deck_add_card_dialog_negative_button, null)
                .create();
    }
}
