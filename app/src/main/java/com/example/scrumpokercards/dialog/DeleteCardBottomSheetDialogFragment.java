package com.example.scrumpokercards.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.scrumpokercards.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DeleteCardBottomSheetDialogFragment extends BottomSheetDialogFragment {

    public interface Listener {
        void onCardDeleted();
    }

    private Listener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (Listener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_delete_card, container, false);
        rootView.setOnClickListener(v -> {
            listener.onCardDeleted();
            dismiss();
        });
        return rootView;
    }
}
