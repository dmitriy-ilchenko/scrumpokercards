package com.example.scrumpokercards.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.widget.GridView;

import com.example.scrumpokercards.dialog.AddCardDialogFragment;
import com.example.scrumpokercards.adapter.CardDeckAdapter;
import com.example.scrumpokercards.dialog.DeleteCardBottomSheetDialogFragment;
import com.example.scrumpokercards.R;
import com.example.scrumpokercards.SettingsManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class CustomCardDeckActivity extends AppCompatActivity implements AddCardDialogFragment.Listener, DeleteCardBottomSheetDialogFragment.Listener {
    private static final String CARD_DELETECTED_FOR_DELETING_KEY = "CARD_DELETECTED_FOR_DELETING_KEY";

    private SettingsManager settingsManager = SettingsManager.getInstance(this);
    private CardDeckAdapter adapter;
    private String cardSelectedForDeleting;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_card_deck);
        initActionBar();
        initCardsGridView();
        initEventHandlers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        settingsManager.setCustomCardDeck(adapter.getData());
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(CARD_DELETECTED_FOR_DELETING_KEY, cardSelectedForDeleting);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        cardSelectedForDeleting = savedInstanceState.getString(CARD_DELETECTED_FOR_DELETING_KEY);
    }


    @Override
    public void onCardAdded(@NonNull String card) {
        adapter.addItem(card);
    }

    @Override
    public void onCardDeleted() {
        adapter.deleteItem(cardSelectedForDeleting);
    }


    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) {
            return;
        }

        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    private void initCardsGridView() {
        // Calculate number of card deck columns
        int cardDeckColumnNumber = 3;
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            cardDeckColumnNumber = 5;
        }

        adapter = new CardDeckAdapter(LayoutInflater.from(this), settingsManager.getCustomCardDeck(), item -> {
            cardSelectedForDeleting = item;
            showDeleteCardDialog();
        });

        GridView cardsGridView = findViewById(R.id.cards_grid_view);
        cardsGridView.setNumColumns(cardDeckColumnNumber);
        cardsGridView.setAdapter(adapter);
    }

    private void initEventHandlers() {
        findViewById(R.id.add_card_fab).setOnClickListener(v -> showAddCardDialog());
    }


    private void showAddCardDialog() {
        new AddCardDialogFragment().show(getSupportFragmentManager(), "AddCardDialogFragment");
    }

    private void showDeleteCardDialog() {
        new DeleteCardBottomSheetDialogFragment().show(getSupportFragmentManager(), "DeleteCardBottomSheetDialogFragment");
    }
}
