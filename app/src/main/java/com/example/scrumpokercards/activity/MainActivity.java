package com.example.scrumpokercards.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.example.scrumpokercards.adapter.CardDeckAdapter;
import com.example.scrumpokercards.R;
import com.example.scrumpokercards.SettingsManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    // Bundle keys
    private static final String TAP_VIEW_VISIBILITY_KEY = "TAP_VIEW_VISIBILITY_KEY";
    private static final String CARD_VIEW_VISIBILITY_KEY = "CARD_VIEW_VISIBILITY_KEY";
    private static final String CARD_TEXT_KEY = "CARD_TEXT_KEY";

    // Views
    private GridView cardsGridView;
    private FloatingActionButton settingsFab;
    private View tapView;
    private View cardView;
    private TextView cardViewTextView;

    // Settings
    private SettingsManager settingsManager;
    private boolean isTapEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        cardsGridView = findViewById(R.id.cards_grid_view);
        settingsFab = findViewById(R.id.settings_fab);
        tapView = findViewById(R.id.tap_view);
        cardView = findViewById(R.id.card_view);
        cardViewTextView = findViewById(R.id.card_view_text_view);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initSettings();
        initCardsGridView();
        initSettingsFab();
        initTapView();
        initCardView();
    }

    @Override
    public void onBackPressed() {
        if (tapView.getVisibility() == View.VISIBLE) {
            tapView.setVisibility(View.GONE);
        } else if (cardView.getVisibility() == View.VISIBLE) {
            cardView.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt(TAP_VIEW_VISIBILITY_KEY, tapView.getVisibility());
        outState.putInt(CARD_VIEW_VISIBILITY_KEY, cardView.getVisibility());
        outState.putString(CARD_TEXT_KEY, cardViewTextView.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int tapViewVisibility = savedInstanceState.getInt(TAP_VIEW_VISIBILITY_KEY);
        tapView.setVisibility(tapViewVisibility);

        int cardViewVisibility = savedInstanceState.getInt(CARD_VIEW_VISIBILITY_KEY);
        cardView.setVisibility(cardViewVisibility);

        String cardText = savedInstanceState.getString(CARD_TEXT_KEY, "");
        cardViewTextView.setText(cardText);
    }


    private void initSettings() {
        settingsManager = SettingsManager.getInstance(getApplicationContext());
        isTapEnabled = settingsManager.isTapEnabled();
    }

    private void initCardsGridView() {
        // Calculate number of card deck columns
        int cardDeckColumnNumber = 3;
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            cardDeckColumnNumber = 5;
        }

        List<String> cardDeck = settingsManager.getCardDeck();

        cardsGridView.setNumColumns(cardDeckColumnNumber);
        cardsGridView.setAdapter(new CardDeckAdapter(LayoutInflater.from(this), cardDeck, this::showCard));
    }

    private void initSettingsFab() {
        settingsFab.setOnClickListener(v -> showSettings());
    }

    private void initTapView() {
        tapView.setOnClickListener(v -> {
            tapView.setVisibility(View.GONE);
            cardView.setVisibility(View.VISIBLE);
        });
    }

    private void initCardView() {
        cardView.setOnClickListener(v -> cardView.setVisibility(View.GONE));
    }


    private void showCard(@NonNull String card) {
        cardViewTextView.setText(card);

        if (isTapEnabled) {
            tapView.setVisibility(View.VISIBLE);
        } else {
            cardView.setVisibility(View.VISIBLE);
        }
    }

    private void showSettings() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }
}
