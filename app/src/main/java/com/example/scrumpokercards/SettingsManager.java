package com.example.scrumpokercards;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;

public class SettingsManager {
    private static final String CUSTOM_CARD_DECK_KEY = "CUSTOM_CARD_DECK_KEY";

    private static final List<String> STANDARD = Arrays.asList("0", "1/2", "1", "2", "3", "5", "8", "13", "20", "40", "100", "∞", "?");
    private static final List<String> FIBONACCI = Arrays.asList("0", "1", "2", "3", "5", "8", "13", "21", "34", "55", "89", "114", "∞", "?");
    private static final List<String> T_SHIRT = Arrays.asList("XS", "S", "M", "L", "XL", "XXL", "∞", "?");

    private static SettingsManager instance;

    private SharedPreferences sharedPreferences;
    private Resources resources;


    private SettingsManager(@NonNull Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        resources = context.getResources();
    }

    public static SettingsManager getInstance(@NonNull Context context) {
        if (instance == null) {
            instance = new SettingsManager(context);
        }
        return instance;
    }

    public boolean isTapEnabled() {
        return sharedPreferences.getBoolean(resources.getString(R.string.preferences_tap_key), false);
    }

    @NonNull
    public List<String> getCardDeck() {
        String value = sharedPreferences.getString(resources.getString(R.string.preferences_card_deck_type_key), "0");
        if (value == null) {
            value = "0";
        }

        int i = Integer.valueOf(value);
        switch (i) {
            case 0: {
                return STANDARD;
            }
            case 1: {
                return FIBONACCI;
            }
            case 2: {
                return T_SHIRT;
            }
            case 3: {
                return getCustomCardDeck();
            }
        }

        throw new IllegalArgumentException();
    }

    @NonNull
    public List<String> getCustomCardDeck() {
        String customCardDeckAsString = sharedPreferences.getString(CUSTOM_CARD_DECK_KEY, "");
        if (customCardDeckAsString == null) {
            customCardDeckAsString = "";
        }
        return convertStringToStringList(customCardDeckAsString);
    }

    public void setCustomCardDeck(@NonNull List<String> customCardDeck) {
        String customCardDeckAsString = convertStringListToString(customCardDeck);
        sharedPreferences.edit()
                .putString(CUSTOM_CARD_DECK_KEY, customCardDeckAsString)
                .apply();
    }


    @NonNull
    private String convertStringListToString(@NonNull List<String> list) {
        if (list.isEmpty()) {
            return "";
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(list.get(0));
        for (String s : list) {
            stringBuilder.append(",");
            stringBuilder.append(s);
        }
        return stringBuilder.toString();
    }

    @NonNull
    private List<String> convertStringToStringList(@NonNull String string) {
        String[] array = string.split(",");
        List<String> result = new ArrayList<>(Arrays.asList(array));
        result.remove(0);
        return result;
    }
}
